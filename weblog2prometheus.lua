function parse_log_line(line)
	local domain,port,source_ip,timestamp,request_line,code,size,referrer,user_agent = line:match("^(.-):([0-9]+) (.-) .- .- %[(.-)%] \"(.-)\" ([0-9]+) ([0-9]+) \"(.-)\" \"(.-)\"")
	method, path, protocol = request_line:match("(.-) (.-) (.-)")
	return domain and {
		domain = domain,
		port = tonumber(port) or 0,
		source_ip = source_ip,
		timestamp = timestamp,
		method = method or "",
		path = path or "",
		protocol = protocol or "",
		code = tonumber(code) or 0,
		size = tonumber(size) or 0,
		referrer = referrer,
		user_agent = user_agent,
	}
end

-- I want to know from my dashboard what's causing all those errors
function categorize_request(request)
	if request.method == "GET" then
		if request.path:match("/atom.xml$") then
			return "feed"
		elseif request.path:match("^/asset") then
			return "asset"
		end
	end
	if request.path:match("^/api/") then
		return "api"
	end
	return "unknown"
end

-- And I want to know roughly what's causing the traffic
function categorize_user_agent(request)
	if request.user_agent:match("Mastodon") or request.user_agent:match("Akkoma") or request.user_agent:match("Pleroma") or request.user_agent:match("Friendica") then
		return "fedi"
	elseif request.user_agent:match("Lieu") or request.user_agent:match("yacybot") then
		return "search.small"
	elseif request.user_agent:match("[Rr][Ss][Ss]") or request.user_agent:match("Newsboat") or request.user_agent:match("Miniflux") then
		return "feedreader"
	elseif request.user_agent:match("[Bb][Oo][Tt]") or request.user_agent:match("Apache%-HttpClient") or request.user_agent:match("python%-requests") or request.user_agent:match("[Cc][Rr][Aa][Ww][Ll][Ee][Rr]") then
		return "bot"
	end
	return "unknown"
end

function add_to_metric(metrics, selector, value)
	metrics[selector] = (metrics[selector] or 0) + (value or 1)
end

function make_metric(mtype, help)
	return { type = mtype, help = help, add = add_to_metric }
end

local metrics = {}

-- currently I'm only interested in requests, but a metric measuring traffic will probably added once I care enough to do so
metrics.http_request_counter = make_metric("counter","The number of http requests by domain, port method and response code")

-- a list of known http methods to make it less likely that someone will generate lots of metrics by litering them.
local okay_http_methods = {
	GET = true,
	POST = true,
	DELETE = true,
	PUT = true,
	HEAD = true,
	OPTIONS = true,
}

-- read logs, accumulate metrics
while true do
	local i = io.read()
	if not i then break end
	local req = parse_log_line(i)
	if req then
		local method = okay_http_methods[req.method] and req.method or "UNKNOWN"
		metrics.http_request_counter:add(('domain="%s",port="%s",code="%s",method="%s",request_category="%s",agent_category="%s"'):format(req.domain, req.port, req.code, method, categorize_request(req), categorize_user_agent(req)))
	else
		--io.stderr:write("Couldn't deal with line: '"..i.."'\n")
	end
end

-- generate metrics file for Prometheus on stdout
for name,metric in pairs(metrics) do
	if metric.help then print("# HELP "..name.." "..metric.help) end
	if metric.type then print("# TYPE "..name.." "..metric.type) end
	for selector, value in pairs(metric) do
		if type(selector) == "string" and type(value) == "number" then
			print(name.."{"..selector.."} "..tostring(value))
		end
	end
end
